﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;

namespace ProofOfConcept.Models {
    public abstract class QuestionType {
        private IEnumerable<Question> _questions;

        public abstract IQuestionValidator MakeQuestionValidator();

        protected abstract IList<Question> MakeQuestions();

        public string Name { get; set; }

        public DynamicBsonDocument Settings { get; set; } = new DynamicBsonDocument();

        public IEnumerable<Question> Questions
        {
            get
            {
                if (_questions == null)
                    _questions = MakeQuestions();
                return _questions;
            }
            set
            {
                _questions = value;
            }
        }

        public IDictionary<string, string> Sources { get; set; } = new Dictionary<string, string>();

        public string Type { get; set; }

 
    }
}