﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Models.Repositories {
    public interface IQuestionTypeRepository {
        IEnumerable<QuestionType> QuestionTypes { get; }

        QuestionType FindQuestionTypeByName(string name);

        void UpdateOrInsertQuestionType(QuestionType type);

        void Rebuild(QuestionType type);

        void RemoveQuestionType(QuestionType type);
    }
}
