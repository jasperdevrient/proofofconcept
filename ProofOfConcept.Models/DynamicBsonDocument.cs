﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Models {
    public class DynamicBsonDocument : DynamicObject {
        private BsonDocument _document = new BsonDocument();

        public override bool TryGetMember(GetMemberBinder binder, out object result) {
            var name = binder.Name;
            if (_document.Contains(name)) {
                result = BsonTypeMapper.MapToDotNetValue(_document[name]);
                return true;
            }
            result = $"Property {name} not defined.";
            return false;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value) {
            if (value is DynamicBsonDocument)
                _document.Set(binder.Name, (value as DynamicBsonDocument).Document);
            else
                _document.Set(binder.Name, BsonTypeMapper.MapToBsonValue(value));
            return true;
        }

        public void SetMember(string name, dynamic value) {
            _document.Set(name, BsonTypeMapper.MapToBsonValue(value));
        }

        public override IEnumerable<string> GetDynamicMemberNames() 
            => _document.Elements.Select(element => element.Name);

        public bool IsLeeg
            => _document.ElementCount == 0;    

        public virtual BsonDocument Document
            => _document;

        public DynamicBsonDocument() { }
        public DynamicBsonDocument(BsonDocument document) { _document = document; }

    }
}
