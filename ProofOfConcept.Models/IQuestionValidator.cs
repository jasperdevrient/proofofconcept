﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Models {
    public interface IQuestionValidator {
        DynamicBsonDocument Validate(Question question, DynamicBsonDocument answer);
    }
}
