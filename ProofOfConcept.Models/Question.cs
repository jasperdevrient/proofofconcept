﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Models {
    public class Question : DynamicBsonDocument {
        public Question() {
            ((dynamic)this).Id = Guid.NewGuid().ToString();
        }

        public Question(BsonDocument document) : base(document) {
            Answer = new DynamicBsonDocument(document["Answer"].AsBsonDocument);
        }

        public DynamicBsonDocument Answer { get; set; } = new DynamicBsonDocument();

        public override BsonDocument Document
        {
            get
            {
                base.Document.Set("Answer", Answer.Document);
                return base.Document;
            }
        }
    }
}
