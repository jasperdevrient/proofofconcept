﻿using ProofOfConcept.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProofOfConcept.Models;
using MongoDB.Driver;
using MongoDB.Bson;
using Ninject;

namespace ProofOfConcept.Services {
    public class QuestionTypeRepository : IQuestionTypeRepository {
        private IMongoCollection<BsonDocument> _collection;
        private IDictionary<string, QuestionType> _questionTypes = new Dictionary<string, QuestionType>();

        [Inject]
        public IMongoCollection<BsonDocument> Collection
        {
           
            set
            {
                _collection = value;
            }
        }

        public IEnumerable<QuestionType> QuestionTypes
        {
            get
            {
                if (_questionTypes.Count != 0)
                    return _questionTypes.Values;
                return _collection.Find(_ => true)
                .ToList().Select(MakeQuestionType);
            }
        }

        private QuestionType MakeQuestionType(BsonDocument questionType) {
            QuestionType qtype = QuestionTypeFactory.BuildQuestionType(questionType);
            _questionTypes.Add(qtype.Name, qtype);
            return qtype;
        }

        public QuestionType FindQuestionTypeByName(string name) {
            if (!_questionTypes.ContainsKey(name)) {
                var results = _collection.Find(Builders<BsonDocument>.Filter.Eq("Name", name))
                    .ToList();

                if (results.Count == 0)
                    throw new IndexOutOfRangeException($"Question type {name} was not found.");
                return MakeQuestionType(results[0].AsBsonDocument);
            }
            return _questionTypes[name];
        }

        public void UpdateOrInsertQuestionType(QuestionType type) {
            var doc = MapToBson(type);
            var filter = Builders<BsonDocument>.Filter.Eq("Name", type.Name);
            var results = _collection.Find(filter)
                    .ToList();
     
            if (results.Count == 0) {
                _collection.InsertOne(doc);
                _questionTypes.Add(type.Name, type);
            } else {
                _collection.ReplaceOne(filter, doc);
                _questionTypes[type.Name] = type;
            }
        }

        public void Rebuild(QuestionType type) { 
            BsonDocument doc = MapToBson(type);
            doc.Remove("Questions");
            var newType = QuestionTypeFactory.BuildQuestionType(doc);
            UpdateOrInsertQuestionType(newType);
        }

        private BsonDocument MapToBson(QuestionType type) {
            dynamic settings = type.Settings;
            settings.ClassName = type.GetType().FullName;
            settings.AssemblyName = type.GetType().Assembly.FullName;
            BsonDocument doc = new BsonDocument();
            doc.Set("Name", BsonTypeMapper.MapToBsonValue(type.Name));
            doc.Set("Settings", type.Settings.Document);
            doc.Set("Questions", new BsonArray(type.Questions.Select(q => q.Document)));
            doc.Set("Type", type.Type);
            doc.Set("Sources", new BsonArray(type.Sources.Select(q => {
                var b = new BsonDocument();
                b.Set("Name", q.Key);
                b.Set("Value", q.Value);
                return b;
            })));
            return doc;
        }

        public void RemoveQuestionType(QuestionType type) {
            _collection.DeleteOne(Builders<BsonDocument>.Filter.Eq("Name", type.Name));
            _questionTypes.Remove(type.Name);
        }
    }
}
