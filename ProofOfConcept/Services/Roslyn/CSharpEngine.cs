﻿using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Services.Roslyn {
    public class CSharpEngine<T> {
        private ScriptState<T> _state = null;
        private ScriptOptions _options = ScriptOptions.Default;

        public void AddImports(params string[] import)
            => _options = _options.AddImports(import);

        public void AddReferences(params Assembly[] assembly)
            => _options = _options.AddReferences(assembly);

        public T Execute(string code) {
            _state = _state == null ? CSharpScript.RunAsync<T>(code, _options).Result : _state.ContinueWithAsync<T>(code, _options).Result;
            if (_state.ReturnValue != null && !string.IsNullOrEmpty(_state.ReturnValue.ToString()))
                return _state.ReturnValue;
            return default(T);
        }

        public T ExecuteFile(string fileName)
            => Execute(System.IO.File.ReadAllText(fileName));
    }
}
