﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace ProofOfConcept.App_Start {
    public class MongoConfig {
        public static MongoClient Client { get; private set; }
        public static IMongoDatabase DataBase { get; private set; }

        public static void OpenConnections() {
            var client = WebConfigurationManager.AppSettings["mongo:ConnectionString"];
            var database = WebConfigurationManager.AppSettings["mongo:Database"];

            Client = new MongoClient(client);
            DataBase = Client.GetDatabase(database);
        }
    }
}
