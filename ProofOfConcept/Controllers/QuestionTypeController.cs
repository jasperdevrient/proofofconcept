﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using ProofOfConcept.Models;
using ProofOfConcept.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProofOfConcept.Controllers {
    public class QuestionTypeController : Controller {

        private IQuestionTypeRepository _questionTypeRepository = MvcApplication.QuestionTypeRepository;

        [HttpGet]
        [Route("QuestionTypes")]
        [AllowJsonGet]
        public JsonResult GetListOfQuestionTypes() {
            try {
                return Json(_questionTypeRepository.QuestionTypes.Select(q => new { q.Name, q.Type }));
            } catch (Exception ex) {
                return MakeError(ex);
            }
        }

        [HttpGet]
        [Route("QuestionType/Natives")]
        [AllowJsonGet]
        public JsonResult GetPosibleNatives() {
            try {
                Type baseType = typeof(QuestionType);
                return Json(GetType().Assembly.GetTypes()
                    .Where(t => baseType.IsAssignableFrom(t))
                    .Select(t => new {
                        Name = t.Name,
                        NameSpace = t.Namespace,
                        ClassName = t.FullName,
                        AssemblyName = t.Assembly.FullName
                    }));
            } catch (Exception ex) {
                return MakeError(ex);
            }
        }

        [HttpPost]
        [AllowJsonGet]
        [Route("QuestionType/Native")]
        public JsonResult MakeQuestionTypeFromNative(string assemblyname, string classname, string name) {
            try {
                dynamic settings = new DynamicBsonDocument();
                settings.ClassName = classname;
                settings.AssemblyName = assemblyname;

                BsonDocument document = new BsonDocument();
                document.Set("Settings", settings.Document);
                document.Set("Name", BsonTypeMapper.MapToBsonValue(name));
                document.Set("Type", BsonTypeMapper.MapToBsonValue("native"));
                var qt = QuestionTypeFactory.BuildQuestionType(document);
                _questionTypeRepository.UpdateOrInsertQuestionType(qt);
                return MakeResult(qt);
            } catch (Exception ex) {
                return MakeError(ex);
            }
        }

        [HttpPost]
        [AllowJsonGet]
        [Route("QuestionType/Python")]
        public JsonResult MakeQuestionTypeFromPython(string name, string source) {
            try {
                BsonDocument document = new BsonDocument();
                document.Set("Name", BsonTypeMapper.MapToBsonValue(name));
                document.Set("Type", BsonTypeMapper.MapToBsonValue("python"));
                BsonArray arr = new BsonArray();
                BsonDocument init = new BsonDocument();
                init.Add("Name", "__init__.py");
                init.Add("Value", source);
                arr.Add(init);
                document.Set("Sources", arr);
                var qt = QuestionTypeFactory.BuildQuestionType(document);
                _questionTypeRepository.UpdateOrInsertQuestionType(qt);
                return MakeResult(qt);
            } catch (Exception ex) {
                return MakeError(ex);
            }
        }

        [HttpPost]
        [AllowJsonGet]
        [Route("QuestionType/CSharp")]
        public JsonResult MakeQuestionTypeFromCSharp(string name, string source) {
            try {
                BsonDocument document = new BsonDocument();
                document.Set("Name", BsonTypeMapper.MapToBsonValue(name));
                document.Set("Type", BsonTypeMapper.MapToBsonValue("csharp"));
                BsonArray arr = new BsonArray();
                BsonDocument init = new BsonDocument();
                init.Add("Name", "index.cs");
                init.Add("Value", source);
                arr.Add(init);
                document.Set("Sources", arr);
                var qt = QuestionTypeFactory.BuildQuestionType(document);
                _questionTypeRepository.UpdateOrInsertQuestionType(qt);
                return MakeResult(qt);
            } catch (Exception ex) {
                return MakeError(ex);
            }
        }

        [HttpPost]
        [AllowJsonGet]
        [Route("QuestionType/{name}/Source")]
        public JsonResult EditSourceFile(string name, string sourcename, string source) {
            try {
                var qt = _questionTypeRepository.FindQuestionTypeByName(name);
                if (qt.Sources.ContainsKey(sourcename))
                    qt.Sources[sourcename] = source;
                else
                    qt.Sources.Add(sourcename, source);

                _questionTypeRepository.UpdateOrInsertQuestionType(qt);
                return MakeResult(qt);
            } catch (IndexOutOfRangeException ex) {
                return MakeError(ex, 404);
            } catch (Exception ex) {
                return MakeError(ex);
            }
        }

        [HttpPost]
        [AllowJsonGet]
        [Route("QuestionType/{name}/Rebuild")]
        public JsonResult RebuildQuestionType(string name) {
            try {
                var qt = _questionTypeRepository.FindQuestionTypeByName(name);
                _questionTypeRepository.Rebuild(qt);
                return MakeResult(_questionTypeRepository.FindQuestionTypeByName(name));
            } catch (IndexOutOfRangeException ex) {
                return MakeError(ex, 404);
            } catch (Exception ex) {
                return MakeError(ex);
            }
        }

        private JsonResult MakeResult(QuestionType qt) {
            var jsonWriterSettings = new JsonWriterSettings { OutputMode = JsonOutputMode.Strict };
            return Json(new {
                Settings = Newtonsoft.Json.Linq.JObject.Parse(qt.Settings.Document.ToJson(jsonWriterSettings)).ToObject(typeof(ExpandoObject)),
                Questions = qt.Questions.Select(q => Newtonsoft.Json.Linq.JObject.Parse(q.Document.ToJson(jsonWriterSettings)).ToObject(typeof(ExpandoObject))),
                qt.Type,
                qt.Name,
                qt.Sources
            });
        }
        
        [HttpDelete]
        [AllowJsonGet]
        [Route("QuestionType/{name}")]
        public JsonResult RemoveQuestionType(string name) {
            try {
                var qt = _questionTypeRepository.FindQuestionTypeByName(name);
                _questionTypeRepository.RemoveQuestionType(qt);
                return Json(new { });
            } catch (IndexOutOfRangeException ex) {
                return MakeError(ex, 404);
            } catch (Exception ex) {
                return MakeError(ex);
            }
        }

        [HttpGet]
        [Route("QuestionType/{name}")]
        [AllowJsonGet]
        public JsonResult GetQuestionType(string name) {
            try {
                return MakeResult(_questionTypeRepository.FindQuestionTypeByName(name));
            } catch (Exception ex) {

                return MakeError(ex, 404);
            }
        }

        private JsonResult MakeError(Exception ex, int statusCode = 500) {
            Response.StatusCode = statusCode;
            Response.StatusDescription = ex.Message;
            return Json(
                new {
                    Error = ex.Message
#if DEBUG
                    ,
                    StackTrace = ex.StackTrace
#endif
                });
        }


    }

    public class AllowJsonGetAttribute : ActionFilterAttribute {
        public override void OnResultExecuting(ResultExecutingContext filterContext) {
            var jsonResult = filterContext.Result as JsonResult;

            if (jsonResult == null)
                throw new ArgumentException("Action does not return a JsonResult, attribute AllowJsonGet is not allowed");

            jsonResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            base.OnResultExecuting(filterContext);
        }
    }
}