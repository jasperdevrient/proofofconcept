﻿using Ninject;
using ProofOfConcept.Models;
using ProofOfConcept.Models.Repositories;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProofOfConcept.Controllers
{
    public class QuestionController : Controller
    {
        private IQuestionTypeRepository _questionTypeRepository = MvcApplication.QuestionTypeRepository;
        // GET: Question
        public ActionResult Index()
        {
            Random r = new Random();
            int c = _questionTypeRepository.QuestionTypes.Count();
            var qt = _questionTypeRepository.QuestionTypes.ElementAt(r.Next(c));
            var q = qt.Questions.ElementAt(r.Next(qt.Questions.Count()));
            Session["Question"] = q;
            Session["Qt"] = qt;
            var viewName = "view";
            string body = "<strong>TEMPLATE NOT FOUND.</strong>";
            if (qt.Sources.ContainsKey(viewName)) {
                try {
                    body = Engine.Razor.RunCompile(qt.Sources[viewName], Guid.NewGuid().ToString(), null, new ScriptViewModel() { Question = q });
                } catch (Exception ex) {
                    body = "<strong>" + ex.Message + "</strong>";
                }
            }
                
            return View("Index", new QuestionViewModel { QuestionHtml = body });
        }
        [HttpPost]
        public ActionResult Validate() {
            var q = (Question)Session["Question"];
            var qt = (QuestionType)Session["Qt"];
            var validator = qt.MakeQuestionValidator();
            var viewName = "view";
            DynamicBsonDocument answer = new DynamicBsonDocument();
            foreach (string k in Request.Form.Keys) {
                double d;
                int i;
                bool b;
                string val = Request.Form[k];
                if (bool.TryParse(val, out b))
                    answer.SetMember(k, b);
                else if (int.TryParse(val, out i))
                    answer.SetMember(k, i);
                else if(double.TryParse(val, out d))
                    answer.SetMember(k, d);
                else
                    answer.SetMember(k, Request.Form[k]);
            }
            dynamic result = new DynamicBsonDocument();
            try {
                 result = validator.Validate(q, answer);
            } catch(Exception ex) {
                result.Message = ex.Message;
#if DEBUG
                result.StackTrace = ex.StackTrace;
#endif
            }

            if (((DynamicBsonDocument)result).Document.ElementCount == 0) {
                // alles correct;
                return RedirectToAction("Index", "Question");
            }
            string body = "<strong>TEMPLATE NOT FOUND.</strong>";
            if (qt.Sources.ContainsKey(viewName)) {
                try {
                    body = Engine.Razor.RunCompile(qt.Sources[viewName], Guid.NewGuid().ToString(), null, new ScriptViewModel() { Question = q, Answer = answer, Result = result });
                } catch (Exception ex) {
                    body = "<strong>" + ex.Message + "</strong>";
                }
            }

            return View("Index", new QuestionViewModel { QuestionHtml = body });
        }
    }

    public class QuestionViewModel {
        public string QuestionHtml { get; set; }
    }

    public class ScriptViewModel {
        public Question Question { get; set; }
        public DynamicBsonDocument Answer { get; set; }
        public DynamicBsonDocument Result { get; set; }
    }
}