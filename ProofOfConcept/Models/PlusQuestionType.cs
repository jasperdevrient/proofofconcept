﻿using System;
using System.Collections.Generic;

namespace ProofOfConcept.Models {
    public class PlusQuestionType : QuestionType {

        public override IQuestionValidator MakeQuestionValidator()
            => new QuestionValidator();

        protected override IList<Question> MakeQuestions() {
            Random r = new Random();
            IList<Question> qs = new List<Question>(40);
            for (int i = 0; i < 40; ++i) {
                int a = r.Next(-100, 101);
                int b = r.Next(-100, 101);
                qs.Add(MakeQuestion(a, b, a + b));
            }
            return qs;
        }

        private Question MakeQuestion(int a, int b, int result) {
            dynamic q = new Question();
            q.A = a;
            q.B = b;
            q.Answer.Result = result;
            return q;
        }

        private class QuestionValidator : IQuestionValidator {
            public DynamicBsonDocument Validate(Question question, DynamicBsonDocument answer) {
                dynamic q = question;
                dynamic a = answer;
                dynamic validationResult = new DynamicBsonDocument();
                if (q.Answer.Result != a.Result) {
                    validationResult.Message = "Wrong answer";
                    validationResult.Answer = q.Answer.Result;
                }
                return validationResult;
            }
        }
    }
}
