﻿using Microsoft.Scripting.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Models {
    [QuestionTypeFactory.Register("python")]
    public class PythonQuestionTypeBuilder : QuestionTypeFactory.IQuestionTypeBuilder {
        private ScriptEngine _engine;

        public PythonQuestionTypeBuilder() {
            _engine =  MvcApplication.PythonEngine;
        }

        public PythonQuestionTypeBuilder(ScriptEngine engine) {
            _engine = engine;
        }

        public QuestionType Build(string name, IDictionary<string, string> sources, dynamic settings, dynamic questions) {
            if (!sources.ContainsKey("__init__.py"))
                throw new Exception("No entrypoint");
            var source = _engine.CreateScriptSourceFromString(sources["__init__.py"].Replace("\t","    "));
            var scope = _engine.CreateScope();
            var compiled = source.Compile();
            compiled.Execute(scope);
            if (scope.ContainsVariable("questionType")) {
                var qt = scope.GetVariable<QuestionType>("questionType");
                if (settings != null)
                    qt.Settings = settings;
                if (questions != null)
                    qt.Questions = questions;
                return qt;
            } else {
                throw new Exception("No variabele questionType set.");
            }
           
        }
    }
}
