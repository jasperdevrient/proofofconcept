﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Models {
    public class QuestionTypeFactory {
        private static Lazy<QuestionTypeFactory> _instance = new Lazy<QuestionTypeFactory>(() => new QuestionTypeFactory());
        private IDictionary<string, IQuestionTypeBuilder> _builders = new Dictionary<string, IQuestionTypeBuilder>();
    
        private QuestionTypeFactory() {
            var basetype = typeof(IQuestionTypeBuilder);
            var types = GetType().Assembly.GetTypes()
                .Where(t => basetype.IsAssignableFrom(t) &&
                t.GetCustomAttributes(typeof(Register), false).Length==  1);
            foreach (var type in types) {
                var attr = type.GetCustomAttributes(typeof(Register), false)[0] as Register;
                var ctor = type.GetConstructor(new Type[] { });
                var obj = ctor.Invoke(null) as IQuestionTypeBuilder;
                AddBuilder(attr.Name, obj);
                Debug.WriteLine($"Builder {attr.Name} added: {obj.GetType().FullName}.");
            }
        }

        public static QuestionTypeFactory Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public QuestionType Build(BsonDocument document) {
            string name = document["Name"].AsString;
            string type = document["Type"].AsString;
            dynamic questions  =document.Contains("Questions") ? document["Questions"].AsBsonArray
               .Values
               .Select(v => new Question(v.AsBsonDocument)) : null;
            dynamic settings = document.Contains("Settings") ? new DynamicBsonDocument(document["Settings"].AsBsonDocument): null;
            if (!_builders.ContainsKey(type))
                throw new IndexOutOfRangeException($"No builder was found for type {type}.");
            IDictionary<string,string> sources = new Dictionary<string,string>();
            if (document.Contains("Sources")) {
                foreach (var source in document["Sources"].AsBsonArray)
                    sources.Add(source["Name"].AsString, source["Value"].AsString);
            }
            QuestionType questiontype = _builders[type].Build(name, sources, settings, questions);
            questiontype.Name = name;
            questiontype.Type = type;
            questiontype.Sources = sources;
            return questiontype;
        }
        

        public QuestionTypeFactory AddBuilder(string name, IQuestionTypeBuilder builder) {
            _builders.Add(name, builder);
            return this;
        }

        public static QuestionType BuildQuestionType(BsonDocument document)
            => Instance.Build(document);

        public static void AddQuestionTypeBuilder(string name, IQuestionTypeBuilder builder)
            => Instance.AddBuilder(name, builder);


        public interface IQuestionTypeBuilder {
            QuestionType Build(string name, IDictionary<string,string> sources, dynamic settings, dynamic questions);
        }

        public class Register : Attribute {
            public string Name { get; private set; }

            public Register(string name) {
                Name = name;
            }
        }
    }

    [QuestionTypeFactory.Register("native")]
    public class NativeQuestionTypeBuilder : QuestionTypeFactory.IQuestionTypeBuilder {
        public QuestionType Build(string name, IDictionary<string, string> sources, dynamic settings, dynamic questions) {
            string className = settings.ClassName;
            string assemblyName = settings.AssemblyName;
            QuestionType qt = (QuestionType)Activator.CreateInstance(assemblyName, className).Unwrap();

            if (questions != null)
                qt.Questions = questions;
            qt.Settings = settings;
            return qt;
        }

    }
}
