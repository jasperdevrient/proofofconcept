﻿using ProofOfConcept.Services.Roslyn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Models {
    [QuestionTypeFactory.Register("csharp")]
    public class CSharpQuestionTypeBuilder : QuestionTypeFactory.IQuestionTypeBuilder {
        private CSharpEngine<QuestionType> _engine;

        public CSharpQuestionTypeBuilder() {
            _engine = MvcApplication.CSharpEngine.Value;
        }

        public CSharpQuestionTypeBuilder(CSharpEngine<QuestionType> engine) {
            _engine = engine;
        }

        public QuestionType Build(string name, IDictionary<string, string> sources, dynamic settings, dynamic questions) {
            const string initFile = "index.cs";
            if (!sources.ContainsKey(initFile))
                throw new IndexOutOfRangeException($"{initFile} was not found.");
            var obj = _engine.Execute(sources[initFile]);

            if (settings != null)
                obj.Settings = settings;
            if (questions != null)
                obj.Questions = questions;
            return obj;
        }
    }
}
