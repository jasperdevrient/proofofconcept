﻿String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

window.qtoverviewController = {
    load: function (c) {
        var self = this;
        $.getJSON("QuestionType/Natives", {}, function (natives) {
            $.getJSON("QuestionTypes", {}, function (data) {
                self.natives = natives;
                self.data = data;
                c();
            });
        });
    },
    init: function () {

    },
    beforeRender: function () {
        return { questiontypes: this.data, natives: this.natives };
    },
    onItemClick: function (data) {
        framework.changeView("#app-view", "editor", data)
    },
    itemRemove: function (data) {
        if (confirm("Are you sure?") == true) {
            $.ajax({
                type: 'DELETE',
                url: "QuestionType/" + data.Name,
                data: '{}',
                success: function (data) { framework.changeView("#app-view", "questiontypesoverview"); },
                contentType: "application/json",
                dataType: 'json'
            });
        }
    },
    newPython: function () {
        var name = undefined;
        while (!name)
            name = prompt("Name");
        $.ajax({
            type: 'POST',
            url: "QuestionType/Python",
            data: JSON.stringify({ name: name, source: "from System.Collections.Generic import List\nfrom ProofOfConcept.Models import *\nclass MyQuestionType(QuestionType):\n\tdef MakeQuestionValidator(self):\n\t\treturn None\n\n\tdef MakeQuestions(self):\n\t\tlist = List[Question]()\n\t\tfor x in xrange(1, 11):\n\t\t\tq = Question()\n\t\t\tq.A = x\n\t\t\tq.B = 7\n\t\t\tq.Answer.Result = x * 7\n\t\t\tlist.Add(q)\n\t\treturn list\n\nquestionType = MyQuestionType()" }),
            success: function (data) { framework.changeView("#app-view", "editor", data); },
            contentType: "application/json",
            dataType: 'json'
        });
    },
    newCSharp: function () {
        var name = undefined;
        while (!name)
            name = prompt("Name");
        $.ajax({
            type: 'POST',
            url: "QuestionType/CSharp",
            data: JSON.stringify({ name: name, source: "using System;\nusing System.Collections.Generic;\nusing ProofOfConcept.Models;\n\npublic class {NAME}QuestionType : QuestionType {\n\tpublic override IQuestionValidator MakeQuestionValidator()\n\t\t=> new QuestionValidator();\n\n\tprotected override IList<Question> MakeQuestions() {\n\t\tRandom r = new Random();\n\t\tIList<Question> qs = new List<Question>(40);\n\t\tfor (int i = 0; i < 10; ++i) {\n\t\t\tint a = r.Next(-100, 101);\n\t\t\tint b = r.Next(-100, 101);\n\t\t\tqs.Add(MakeQuestion(a, b, a + b));\n\t\t}\n\t\treturn qs;\n\t}\n\tprivate Question MakeQuestion(int a, int b, int result) {\n\t\tdynamic q = new Question();\n\t\tq.A = a;\n\t\tq.B = b;\n\t\tq.Answer.Result = result;\n\t\treturn q;\n\t}\n\n\tprivate class QuestionValidator : IQuestionValidator {\n\t\tpublic DynamicBsonDocument Validate(Question question, DynamicBsonDocument answer) {\n\t\t\tdynamic q = question;\n\t\t\tdynamic a = answer;\n\t\t\tdynamic validationResult = new DynamicBsonDocument();\n\t\t\tif (q.Answer.Result != a.Result) { \n\t\t\t\tvalidationResult.Message = \"Wrong answer\";\n\t\t\t\tvalidationResult.Answer = q.Answer.Result;\n\t\t\t}\n\t\t\treturn validationResult;\n\t\t}\n\t}\n}\n\nreturn new {NAME}QuestionType();".replaceAll("{NAME}", name) }),
            success: function (data) { framework.changeView("#app-view", "editor", data); },
            contentType: "application/json",
            dataType: 'json'
        });
    },
    newNative: function (native) {
        var name = undefined;
        while (!name)
            name = prompt("Name");
        native.Name = name;
        $.ajax({
            type: 'POST',
            url: "QuestionType/Native",
            data: JSON.stringify(native),
            success: function (data) { framework.changeView("#app-view", "editor", data); },
            contentType: "application/json",
            dataType: 'json'
        });
    }

}

window.editorController = {
    load: function (c) {
        if (this.arguments.Sources === undefined) {
            var self = this;
            $.getJSON("QuestionType/" + this.arguments.Name, {}, function (data) {
                self.data = data;
                c();
            });
        } else {
            this.data = this.arguments;
            c();
        }
    },
    beforeRender: function () {
        return this.data;
    },
    rebuild: function (element) {
        $.post("QuestionType/" + this.arguments.Name + "/Rebuild", JSON.stringify({}), function (data) {
            framework.changeView("#app-view", "editor", data);
        }).fail(function (req, type, msg) {
            framework.changeView("#app-error", "error", { message: msg });
        });
    },
    onItemClick: function (d) {
        var k = this.data.Sources[d.name];
        framework.events.trigger("editorselect", { value: k, name: d.name });
        this.selected = d.name;
    },
    save: function () {
        framework.events.trigger("editorsave", { name: this.data.Name, sourcename: this.selected });
    },
    newFile: function () {
        var name = undefined;
        while (!name) {
            name = prompt("Filename");
        }
        $.ajax({
            type: 'POST',
            url: "QuestionType/" + this.data.Name + "/Source",
            data: JSON.stringify({ sourcename: name, source: "" }),
            success: function (data) { framework.changeView("#app-view", "editor", data); },
            contentType: "application/json",
            dataType: 'json'
        });
    }
}

window.codeController = {
    init: function () {
        framework.events.subscribe(this, "editorselect", this.loadCode.bind(this));
        framework.events.subscribe(this, "editorsave", this.saveCode.bind(this));
    },
    loadCode: function (d) {
        var k = d.name;
        window.editor.setValue(d.value);
        if (k.endsWith('.py'))
            window.editor.setOption('mode', 'python');
        else if (k.endsWith('.cs'))
            window.editor.setOption('mode', 'text/x-csharp');
        else
            window.editor.setOption('mode', 'razor');
    },
    saveCode: function (o) {
        var content = window.editor.getValue();
        $.ajax({
            type: 'POST',
            url: "QuestionType/" + o.name + "/Source",
            data: JSON.stringify({ sourcename: o.sourcename, source: content }),
            success: function (data) { framework.changeView("#app-view", "editor", data); },
            contentType: "application/json",
            dataType: 'json'
        });
    },
    afterRender: function () {
        window.editor = CodeMirror.fromTextArea(document.getElementById("ta-content"), {
            lineNumbers: true,
            mode: "python",
            matchBrackets: true,
            theme: 'neat',
            viewportMargin: 20,
            extraKeys: {
                "F11": function (cm) {
                    cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                },
                "Esc": function (cm) {
                    if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                }
            }
        });
    }
};

window.errorController = {
    beforeRender: function () {
        return this.arguments;
    }
};