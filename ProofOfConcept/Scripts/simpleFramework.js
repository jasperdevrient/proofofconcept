﻿var views = {};
var renderedViews = {};
var actions = [];
var actionData = {};

window.framework = {
    events: new EventBus("test"),
    guid: function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x100000)
              .toString(16)
              .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4();
    },
    getEventBus: function (element) {
        return renderedViews[element].events;
    },
    changeView: function (element, view, params) {
        views[view].renderTo(element, params);
    },
    addAction: function (action) {
        actions.push(action);
    },
    action: function (actionname, event, data) {
        var g = framework.guid();
        actionData[g] = data;
        return "action-" + actionname + '="' + event + '" data="' + g + '"';
    }

};

$(function () {
    framework.addAction({
        name: 'click',
        handle: function (element, attributedata, data, controller) {
            var fn = _.identity;
            if (controller[attributedata] !== undefined)
                var fn = controller[attributedata].bind(controller);
            $(element).unbind('click').click(function (ev) {
                if (data !== undefined)
                    fn(data, ev, element);
                else
                    fn(ev, element);
            });
        }
    });


    $('script[type="text/template"]').each(function (i, el) {
        var name = $(el).attr("name");
        var controller = window[$(el).attr("controller")];
        var content = $(el).html();
        $(el).remove();
        var view = new View(name, content, controller);
        views[name] = view;
    })

    $("[render]").each(function (i, el) {
        var view = views[$(el).attr("render")];
        view.renderTo(el);
    });
    console.log("App ready");
});

function View(name, template, controller) {
    this.name = name;
    this.template = _.template(template);
    this.controller = controller;
    if (controller === undefined)
        this.controller = {};
    var self = this;
    this.renderTo = function (element, params) {
        var controller = _.cloneDeep(self.controller)
        var events = new EventBus(element);
        if (renderedViews[element] !== undefined) {
            framework.events.release(renderedViews[element].controller);
        }
        renderedViews[element] = { controller: controller, events: events };

        controller.self = self;
        controller.events = events;
        controller.element = element;
        controller.arguments = params;
        controller.guid = framework.guid();
        var c = function () {
            if (controller.init !== undefined)
                controller.init();
            var variables = {};
            if (controller.beforeRender !== undefined)
                variables = controller.beforeRender.bind(controller)();
            $(element).html(self.template(variables));
            _.each(actions, function (action) {
                $(element).find('[action-' + action.name + ']').each(function (i, el) {
                    var data = actionData[$(el).attr("data")];
                    action.handle(el, $(el).attr('action-' + action.name), data, controller);
                });
            });
            $(element).find("[render]").each(function (i, el) {
                var view = views[$(el).attr("render")];
                view.renderTo(el);
            });
            if (controller.afterRender !== undefined)
                controller.afterRender.bind(controller)($(element));
        };
        if (controller.load !== undefined)
            controller.load.bind(controller)(c);
        else
            c();
    }
}

function EventBus(element) {
    var events = {};

    this.subscribe = function (subject, name, fn) {
        if (events[name] === undefined)
            events[name] = {};
        events[name][subject.guid] = fn;
    };

    this.release = function (subject) {
        var c = 0;
        _.each(events, function (event) {
            if (event[subject.guid] !== undefined) {
                delete event[subject.guid];
                ++c;
            }
        });
        console.log(subject.guid + " released from " + c + " targets.");
    }

    this.trigger = function (name, options) {
        if (events[name] !== undefined)
            _.each(events[name], function (fn) {

                fn(options);
            });
    };
}