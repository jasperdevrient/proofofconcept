﻿using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using ProofOfConcept.App_Start;
using ProofOfConcept.Models;
using ProofOfConcept.Models.Repositories;
using ProofOfConcept.Services;
using ProofOfConcept.Services.Roslyn;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ProofOfConcept {
    public class MvcApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            Application.Clear();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MongoConfig.OpenConnections();

            if (!Application.AllKeys.Contains("Service"))
                Application.Set("Service", new QuestionTypeRepository());
            QuestionTypeRepository = (IQuestionTypeRepository)Application["Service"];
            ((QuestionTypeRepository)QuestionTypeRepository).Collection = MongoConfig.DataBase.GetCollection<MongoDB.Bson.BsonDocument>("QuestionTypes");
            if (!Application.AllKeys.Contains("Python")) {
                var engine = Python.CreateEngine();
                engine.Runtime.LoadAssembly(typeof(IQuestionTypeRepository).Assembly);
                engine.Runtime.LoadAssembly(typeof(BsonDocument).Assembly);
                Application.Set("Python", engine);
            }
            PythonEngine = (ScriptEngine)Application["Python"];
            CSharpEngine = new Lazy<CSharpEngine<QuestionType>>(() => {
                const string key = "CSharp";
                if (!Application.AllKeys.Contains(key)) {
                    var engine = new CSharpEngine<QuestionType>();
                    engine.AddReferences(typeof(ExpandoObject).Assembly,
                    typeof(Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo).Assembly,
                    typeof(Action).Assembly,
                    typeof(QuestionType).Assembly,
                    typeof(BsonDocument).Assembly);
                    engine.AddImports("System", "System.Dynamic");
                    Application[key] = engine;
                }
                return Application[key] as CSharpEngine<QuestionType>;
            });

        }

        public static IQuestionTypeRepository QuestionTypeRepository
        {
            get; private set;
        }

        public static ScriptEngine PythonEngine
        {
            get; private set;
        }

        public static Lazy<CSharpEngine<QuestionType>> CSharpEngine { get; private set; }

    }
}
