﻿using ProofOfConcept.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Performance {
    public class CSharpTestQuestionType : QuestionType {
      

        public override IQuestionValidator MakeQuestionValidator()
            => new QuestionValidator();

        protected override IList<Question> MakeQuestions() {
            IList<Question> qs = new List<Question>();
            for (int x = 1; x <= 15; ++x)
                for (int y = 1; y <= 10; ++y) {
                    dynamic q = new Question();
                    q.A = x;
                    q.B = y;
                    q.Answer.Result = x * y;
                    qs.Add(q as Question);
                }
            return qs;
        }

        private class QuestionValidator : IQuestionValidator {
            public DynamicBsonDocument Validate(Question question, DynamicBsonDocument answer) {
                dynamic q = question;
                dynamic a = answer;
                dynamic validationResult = new DynamicBsonDocument();
                if (q.Answer.Result != a.Result) {
                    validationResult.Message = $"{a.Result} was fout!";
                    validationResult.Correct = q.Answer.Result;
                }
                return validationResult;
            }
        }
    }
}
