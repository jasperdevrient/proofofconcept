﻿using IronPython.Hosting;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.Scripting.Hosting;
using MongoDB.Bson;
using ProofOfConcept.Models;
using ProofOfConcept.Models.Repositories;
using ProofOfConcept.Performance.Models;
using ProofOfConcept.Services.Roslyn;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProofOfConcept.Performance {
    class Program {
        static void Main(string[] args) {
            Context = new EFContext();
           
            new Program().Run(args);

            Console.ReadKey();
        }


        private dynamic GenerateAddMethodPython() {
            var python = _pythonEngine.Value;
            var scope = python.CreateScope();
            var additionPython = python.CreateScriptSourceFromString("def add(a, b):\n\treturn a + b").Compile();
            additionPython.Execute(scope);
            return scope.GetVariable("add");
        }

        private async Task<Func<int, int, int>> GenerateAddCSharp() {
            var c = CSharpScript.Create<Func<int, int, int>>("return (int a, int b) => a + b;", ScriptOptions.Default.AddReferences(typeof(Func<int, int, int>).Assembly));
            c.Compile();
            var r = await c.RunAsync();
            return r.ReturnValue;
        }

        private async Task<Action<TQuestion>> GenerateObjectManCSharp() {
            var c = CSharpScript.Create<Action<TQuestion>>("return (TQuestion q) => {q.Answer.Result = q.A * q.B; q.Answer.Result++;};", ScriptOptions.Default.AddReferences(typeof(Func<int, int, int>).Assembly)
                .AddReferences(typeof(TQuestion).Assembly).AddImports("ProofOfConcept.Performance"));
            c.Compile();
            var r = await c.RunAsync();
            return r.ReturnValue;
        }

        private dynamic GenerateObjectManPython() {
            var python = _pythonEngine.Value;
            var scope = python.CreateScope();
            python.Runtime.LoadAssembly(typeof(TQuestion).Assembly);
            var a = python.CreateScriptSourceFromString("from ProofOfConcept.Performance import *\n\ndef man(o):\n\to.Answer.Result = o.A * o.B\n\to.Answer.Result += 1");
            a.Execute(scope);
            return scope.GetVariable("man");
        }

        private async void RunExperiments(int time, int compiletime) {

            var addPython = GenerateAddMethodPython();
            var addCSharp = await GenerateAddCSharp();
            Func<int, int, int> add = (int a, int b) => a + b;
            Func<dynamic, dynamic, dynamic> addDynamic = (dynamic a, dynamic b) => a + b;

            var er = new ExperimentRunner();
             er.StartGroup("Addition")
                 .AddTimeLimitedExperiment("static", () => {
                     add(2, 3);
                 }, time)
                 .AddTimeLimitedExperiment("dynamic", () => {
                     addDynamic(2, 3);
                 }, time)
                 .AddTimeLimitedExperiment("python", () => {
                     addPython(2, 3);
                 }, time)
                 .AddTimeLimitedExperiment("csharp", () => {
                     addCSharp(2, 3);
                 }, time)
                 .EndGroup()
                 .Group("Validation faulty", (eb) => {
                     var pythonQt = new PythonQuestionTypeBuilder(_pythonEngine.Value).Build("python", new Dictionary<string, string>() {
                         {"__init__.py", File.ReadAllText("Scripts/Tafels.py") }
                     }, null, null);
                     var csharpQt = new CSharpQuestionTypeBuilder(_csharpEngineQt.Value).Build("csharp", new Dictionary<string, string>() {
                         {"index.cs", File.ReadAllText("Scripts/Tafels.csx") }
                     }, null, null);
                     var nativeDynamic = new CSharpTestQuestionType();
                     var nativeStaticAnswer = new TAnswer() { Result = 4 };
                     var nativeStaticQuestion = new TQuestion() { Answer = new TAnswer() { Result = 3 } };
                     var pythonVal = pythonQt.MakeQuestionValidator();
                     var csharpVal = csharpQt.MakeQuestionValidator();
                     var nativeVal = nativeDynamic.MakeQuestionValidator();
                     var q = csharpQt.Questions.ElementAt(3);
                     dynamic answer = new DynamicBsonDocument();
                     answer.Result = 5;
                     eb.AddTimeLimitedExperiment("static", () => {
                         Validate(nativeStaticAnswer, nativeStaticQuestion);
                     }, time);
                     eb.AddTimeLimitedExperiment("dynamic", () => {
                         nativeVal.Validate(q, answer);
                     }, time);
                     eb.AddTimeLimitedExperiment("csharp", () => {
                         csharpVal.Validate(q, answer);
                     }, time);
                     eb.AddTimeLimitedExperiment("python", () => {
                         pythonVal.Validate(q, answer);
                     }, time);
                 })
                 .Group("Validation correct", (eb) => {
                     var pythonQt = new PythonQuestionTypeBuilder(_pythonEngine.Value).Build("python", new Dictionary<string, string>() {
                         {"__init__.py", File.ReadAllText("Scripts/Tafels.py") }
                     }, null, null);
                     var csharpQt = new CSharpQuestionTypeBuilder(_csharpEngineQt.Value).Build("csharp", new Dictionary<string, string>() {
                         {"index.cs", File.ReadAllText("Scripts/Tafels.csx") }
                     }, null, null);
                     var nativeDynamic = new CSharpTestQuestionType();
                     var nativeStaticAnswer = new TAnswer() { Result = 4 };
                     var nativeStaticQuestion = new TQuestion() { Answer = new TAnswer() { Result = 3 } };
                     var pythonVal = pythonQt.MakeQuestionValidator();
                     var csharpVal = csharpQt.MakeQuestionValidator();
                     var nativeVal = nativeDynamic.MakeQuestionValidator();
                     var q = csharpQt.Questions.ElementAt(3);
                     dynamic answer = new DynamicBsonDocument();
                     answer.Result = 4;
                     eb.AddTimeLimitedExperiment("static", () => {
                         Validate(nativeStaticAnswer, nativeStaticQuestion);
                     }, time);
                     eb.AddTimeLimitedExperiment("dynamic", () => {
                         nativeVal.Validate(q, answer);
                     }, time);
                     eb.AddTimeLimitedExperiment("csharp", () => {
                         csharpVal.Validate(q, answer);
                     }, time);
                     eb.AddTimeLimitedExperiment("python", () => {
                         pythonVal.Validate(q, answer);
                     }, time);
                 })
                 .Group("Make questions", (eb) => {
                     TQuiz t = new TQuiz();
                     var pythonQt = new PythonQuestionTypeBuilder(_pythonEngine.Value).Build("python", new Dictionary<string, string>() {
                         {"__init__.py", File.ReadAllText("Scripts/Tafels.py") }
                     }, null, null);
                     var csharpQt = new CSharpQuestionTypeBuilder(_csharpEngineQt.Value).Build("csharp", new Dictionary<string, string>() {
                         {"index.cs", File.ReadAllText("Scripts/Tafels.csx") }
                     }, null, null);
                     var nativeDynamic = new CSharpTestQuestionType();
                     eb.AddTimeLimitedExperiment("static", () => {
                         t.Questions = null;
                         t.Questions = MakeTafels();
                         t.Questions.First();
                     }, time);
                     eb.AddTimeLimitedExperiment("dynamic", () => {
                         nativeDynamic.Questions = null;
                         nativeDynamic.Questions.First();
                     }, time);
                     eb.AddTimeLimitedExperiment("python", () => {
                         pythonQt.Questions = null;
                         pythonQt.Questions.First();
                     }, time);
                     eb.AddTimeLimitedExperiment("csharp", () => {
                         csharpQt.Questions = null;
                         csharpQt.Questions.First();
                     }, time);
                 })
                 .Group("Make validator", (eb) => {
                     var pythonQt = new PythonQuestionTypeBuilder(_pythonEngine.Value).Build("python", new Dictionary<string, string>() {
                         {"__init__.py", File.ReadAllText("Scripts/Tafels.py") }
                     }, null, null);
                     var csharpQt = new CSharpQuestionTypeBuilder(_csharpEngineQt.Value).Build("csharp", new Dictionary<string, string>() {
                         {"index.cs", File.ReadAllText("Scripts/Tafels.csx") }
                     }, null, null);
                     var nativeDynamic = new CSharpTestQuestionType();
                     eb.AddTimeLimitedExperiment("dynamic", () => {
                         nativeDynamic.MakeQuestionValidator();
                     }, time);
                     eb.AddTimeLimitedExperiment("python", () => {
                         pythonQt.MakeQuestionValidator().ToString();
                     }, time);
                     eb.AddTimeLimitedExperiment("csharp", () => {
                         csharpQt.MakeQuestionValidator();
                     }, time);
                 })
                 .Group("Static object manipulations", async (eb) => {
                     TQuestion a = new TQuestion();
                     a.A = 3;
                     a.B = 3;
                     a.Answer = new TAnswer();
                     Action<TQuestion> staticMan = (q) => { q.Answer.Result = q.A * q.B; q.Answer.Result ++; };
                     Action<dynamic> dynamicMan = (q) => { q.Answer.Result = q.A * q.B; q.Answer.Result++; };
                     Action<TQuestion> csharp = await GenerateObjectManCSharp();
                     dynamic python = GenerateObjectManPython();
                     eb.AddTimeLimitedExperiment("static", () => staticMan(a), time);
                     a = new TQuestion();
                     a.A = 3;
                     a.B = 3;
                     a.Answer = new TAnswer();
                     eb.AddTimeLimitedExperiment("dynamic", () => dynamicMan(a), time);
                     a = new TQuestion();
                     a.A = 3;
                     a.B = 3;
                     a.Answer = new TAnswer();
                     eb.AddTimeLimitedExperiment("python", () => python(a), time);
                     a = new TQuestion();
                     a.A = 3;
                     a.B = 3;
                     a.Answer = new TAnswer();
                     eb.AddTimeLimitedExperiment("csharp", () => csharp(a), time);
                 })
                 .Group("Compile test add", (eb) => {
                     eb.AddTimeLimitedExperiment("python", () => GenerateAddMethodPython(), compiletime);
                     eb.AddTimeLimitedExperiment("csharp", async () => await GenerateAddCSharp(), compiletime);
                 })
                 .Group("Compile test object man", (eb) => {
                     eb.AddTimeLimitedExperiment("python", () => GenerateObjectManPython(), compiletime);
                     eb.AddTimeLimitedExperiment("csharp", async () => await GenerateObjectManCSharp(), compiletime);
                 })
                 .Group("Compile test Questiontypes", (eb) => {
                     var pythonBuilder = new PythonQuestionTypeBuilder(_pythonEngine.Value);

                     Action pythonQtA = () => pythonBuilder.Build("python", new Dictionary<string, string>() {
                         {"__init__.py", File.ReadAllText("Scripts/Tafels.py") }
                     }, null, null);
                     var csharpBuilder = new CSharpQuestionTypeBuilder(_csharpEngineQt.Value);

                     Action csharpQtA = () => csharpBuilder.Build("csharp", new Dictionary<string, string>() {
                         {"index.cs", File.ReadAllText("Scripts/Tafels.csx") }
                     }, null, null);

                     eb.AddTimeLimitedExperiment("python", pythonQtA, compiletime);
                     eb.AddTimeLimitedExperiment("csharp", csharpQtA, compiletime);
                 })
                 .Run(15000);
        }

        public async void RunParrallel(int time) {
            var addPython = GenerateAddMethodPython();
            var addCSharp = await GenerateAddCSharp();
            Func<int, int, int> add = (int a, int b) => a + b;
            Func<dynamic, dynamic, dynamic> addDynamic = (dynamic a, dynamic b) => a + b;
            var er = new ExperimentRunner();

            er
                .Group("Make questions", (eb) => {
                    TQuiz t = new TQuiz();
                    var pythonQt = new PythonQuestionTypeBuilder(_pythonEngine.Value).Build("python", new Dictionary<string, string>() {
                         {"__init__.py", File.ReadAllText("Scripts/Tafels.py") }
                    }, null, null);
                    var csharpQt = new CSharpQuestionTypeBuilder(_csharpEngineQt.Value).Build("csharp", new Dictionary<string, string>() {
                         {"index.cs", File.ReadAllText("Scripts/Tafels.csx") }
                    }, null, null);
                   
                    var nativeDynamic = new CSharpTestQuestionType();
                    eb.AddTimeLimitedExperiment("static", () => {
                        lock (t) {
                            t.Questions = null;
                            t.Questions = MakeTafels();
                            t.Questions.First();
                        }
                    }, time);
                    eb.AddTimeLimitedExperiment("dynamic", () => {
                        lock (nativeDynamic) {
                            nativeDynamic.Questions = null;
                            nativeDynamic.Questions.First();
                        }
                    }, time);
                    //eb.AddTimeLimitedExperiment("python", () => {
                    //    pythonQt.Questions = null;
                    //    pythonQt.Questions.First();
                    //}, time);

                    eb.AddTimeLimitedExperiment("csharp", () => {
                        lock (csharpQt) {
                            csharpQt.Questions = null;
                            csharpQt.Questions.First();
                        }
                    }, time);
                });
            er.RunGroupExperimentsParrallel(1000, "Make questions");
        }
       public static EFContext Context { get; private set; }
        public async void Run(string[] args) {
           int[] times =  new [] { 1000, 5000, 15000, 30000, 60000 };
           const int compiletime = 1000;
            Console.CancelKeyPress += Console_CancelKeyPress;
            int count = -1;
            if (args.Length == 1 && int.TryParse(args[0], out count)) {
                Console.WriteLine($"Doing {count} groups.");
            }

            RunParrallel(5000);
            int i = 0;
           //while (true) {
           //     Console.WriteLine("Begin experiment group.");
           //     GC.Collect();
           //     foreach (var time in times) {
           //         Console.WriteLine($"\tBegin experiment with time {time}.");
           //         RunExperiments(time, compiletime);
           //         Console.WriteLine($"\tEnd experiment with time {time}.");
           //     }
           //     GC.Collect();
           //     Console.WriteLine("End experiment group.");
           //     if (count != -1) {
           //         i++;
           //         if (i >= count)
           //             break;
           //     }
           // }


            Context.SaveChanges();

        }

        private void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e) {
            Context.SaveChanges();
            Environment.Exit(0);
        }

        private long Test1(Question q, DynamicBsonDocument a, IQuestionValidator v) {
            long r = 0;
            CancellationTokenSource cts = new CancellationTokenSource();
            cts.CancelAfter(30 * 1000);
            CancellationToken ct = cts.Token;
            while (!ct.IsCancellationRequested) {
                v.Validate(q, a);
                ++r;
            }
            return r;
        }

        private long Test2(TQuestion q, TAnswer a) {
            long r = 0;
            CancellationTokenSource cts = new CancellationTokenSource();
            cts.CancelAfter(30 * 1000);
            CancellationToken ct = cts.Token;
            while (!ct.IsCancellationRequested) {
                Validate(a, q);
                ++r;
            }
            return r;
        }

        private long Test3(dynamic user, dynamic question) {
            long r = 0;
            CancellationTokenSource cts = new CancellationTokenSource();
            cts.CancelAfter(30 * 1000);
            CancellationToken ct = cts.Token;
            while (!ct.IsCancellationRequested) {
                Validate(user, question);
                ++r;
            }
            return r;
        }

        private dynamic Validate(dynamic user, dynamic question) {
            dynamic expando = new ExpandoObject();
            if (user.Result != question.Answer.Result) {
                expando.Message = $"{user.Result} was fout!";
                expando.Correct = question.Answer.Result;
            }
            return expando;
        }
        private Result Validate(TAnswer user, TQuestion question) {
            Result r = new Result();
            if (user.Result != question.Answer.Result) {
                r.Message = $"{user.Result} was fout!";
                r.Correct = question.Answer.Result;
            }
            return r;
        }

        private class Result {
            public string Message { get; set; }
            public int Correct { get; set; }
        }

        private IList<TQuestion> MakeTafels() {
            var l = new List<TQuestion>();
            for (int i = 1; i <= 15; ++i)
                for (int j = 1; j <= 10; ++j)
                    l.Add(new TQuestion() { A = i, B = j, Answer = new TAnswer() { Result = i * j } });
            return l;
        }

        private Lazy<ScriptEngine> _pythonEngine = new Lazy<ScriptEngine>(() => {
            var engine = Python.CreateEngine();
            engine.Runtime.LoadAssembly(typeof(IQuestionTypeRepository).Assembly);
            engine.Runtime.LoadAssembly(typeof(BsonDocument).Assembly);
            return engine;
        });
        private class TQuiz {
            public IEnumerable<TQuestion> Questions { get; set; }
        }
        private Lazy<CSharpEngine<object>> _csharpEngine = new Lazy<CSharpEngine<object>>(() => {
            var engine = new CSharpEngine<object>();
            engine.AddReferences(typeof(ExpandoObject).Assembly,
            typeof(Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo).Assembly,
            typeof(Action).Assembly,
            typeof(QuestionType).Assembly,
            typeof(BsonDocument).Assembly);
            engine.AddImports("System", "System.Dynamic");
            return engine;
        });
        private Lazy<CSharpEngine<QuestionType>> _csharpEngineQt = new Lazy<CSharpEngine<QuestionType>>(() => {
            var engine = new CSharpEngine<QuestionType>();
            engine.AddReferences(typeof(ExpandoObject).Assembly,
            typeof(Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo).Assembly,
            typeof(Action).Assembly,
            typeof(QuestionType).Assembly,
            typeof(BsonDocument).Assembly);
            engine.AddImports("System", "System.Dynamic");
            return engine;
        });
    }

    public class TQuestion {
        public int A { get; set; }
        public int B { get; set; }
        public TAnswer Answer { get; set; }
    }
    public class TAnswer {
        public int Result { get; set; }
    }

}
