﻿using ProofOfConcept.Performance.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProofOfConcept.Performance {
    public class Experiment {
        public ulong RunnedExperiments { get; private set; }
        public int TimeLimit { get; set; } = -1;
        public int Count { get; set; } = -1;
        public Stopwatch Stopwatch { get; private set; } = new Stopwatch();

        public string Name { get; set; }

        public void Run() {
            ulong c = 0;
            Stopwatch.Reset();
            var re = RunExperiment;
            if (TimeLimit > -1) {
                Stopwatch.Start();
                CancellationTokenSource cts = new CancellationTokenSource();
                cts.CancelAfter(TimeLimit);
                CancellationToken ct = cts.Token;
                while (!cts.IsCancellationRequested) {
                    re();
                    ++c;
                }
                RunnedExperiments = c;
                Stopwatch.Stop();
            } else {
                Stopwatch.Start();
                for (int current = 0; current < Count; ++current)
                    re();
                Stopwatch.Stop();
            }
        }

        public ExperimentResult[] RunParrallel(int threadCount) {
            Func<ExperimentResult> a = () => {
                ExperimentResult er = new ExperimentResult();
                er.ExperemimentName = Name;
                CancellationTokenSource cts = new CancellationTokenSource();
                cts.CancelAfter(TimeLimit);
                CancellationToken ct = cts.Token;
                long c = 0;
                var s = new Stopwatch();
                var re = RunExperiment;
                if (TimeLimit > -1) {
                    s.Start();
                    while (!cts.IsCancellationRequested) {
                        re();
                        ++c;
                    }
                    s.Stop();
                    er.ExecutionTimeMilliseconds = s.Elapsed.TotalMilliseconds;
                    er.ExecutionTimeSeconds = s.Elapsed.TotalSeconds;
                    er.ExperimentCount = c;
                }
                Console.WriteLine("Done!");
                return er;
            };
            ExperimentResult[] results = new ExperimentResult[threadCount];
            Parallel.ForEach(Enumerable.Range(0, threadCount), (i) => {
                results[i] = a();
            });
            return results;
        }
        public Action RunExperiment { get; set; }
    }
}
