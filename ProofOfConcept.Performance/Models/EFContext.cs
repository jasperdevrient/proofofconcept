﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Performance.Models {
   public  class EFContext : DbContext{
        public EFContext() : base("DefaultConnection") {}

        public DbSet<ExperimentResult> ExperimentResults { get; set; }

        public DbSet<PExperimentResult> PResults { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }

        public void SaveChanges()
            => base.SaveChanges();

        public void ReportResult(ExperimentResult result) {
            ExperimentResults.Add(result);
            Console.WriteLine($"Reported {result}.");
            SaveChanges();
        }
        public void ReportPResult(PExperimentResult result) {
            PResults.Add(result);
            Console.WriteLine($"Reported {result}.");
            SaveChanges();
        }
    }
}
