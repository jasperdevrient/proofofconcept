﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProofOfConcept.Performance.Models {
    public class ExperimentResult {
        public long ExperimentResultId { get; set; }
        public string ExperemimentName { get; set; }
        public string Group { get; set; }
        public double ExecutionTimeSeconds { get; set; }
        public double ExecutionTimeMilliseconds { get; set; }
        public double ExecutionTime { get; set; }
        public long ExperimentCount { get; set; }
    }

    public class PExperimentResult {
        public long PExperimentResultId { get; set; }
        public string ExperemimentName { get; set; }
        public string Group { get; set; }
        public double ExecutionTimeSeconds { get; set; }
        public double ExecutionTimeMilliseconds { get; set; }
        public double ExecutionTime { get; set; }
        public long ExperimentCount { get; set; }
    }
}
