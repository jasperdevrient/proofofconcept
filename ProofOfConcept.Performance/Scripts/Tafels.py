﻿from System.Collections.Generic import List
from ProofOfConcept.Models import *

class MyQuestionType(QuestionType):
    def MakeQuestionValidator(self):
        return MyValidator()
      
      
    def MakeQuestions(self):
        list = List[Question]()
        for x in xrange(1, 16):
             for y in xrange(1, 11):
                  q = Question()
                  q.A = x
                  q.B = y
                  q.Answer.Result = x * y
                  list.Add(q)
        return list

class MyValidator(IQuestionValidator):
    def Validate(self, question, answer):
        res = DynamicBsonDocument()
        if (question.Answer.Result != answer.Result):
            res.Message = str(answer.Result) + " was fout!"
            res.Correct = question.Answer.Result

        return res

questionType = MyQuestionType()