﻿using ProofOfConcept.Performance.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProofOfConcept.Performance {
    public class ExperimentRunner {
        private IList<ExperimentGroup> _groups = new List<ExperimentGroup>();
        private class ExperimentGroup {
            public string Name { get; set; }
            public IEnumerable<Experiment> Experiments { get; set; }
        }
        public class ExperimentGroupBuilder {
            private string _name;
            private ExperimentRunner _runner;
            private IList<Experiment> _experiments = new List<Experiment>();
            public ExperimentGroupBuilder AddTimeLimitedExperiment(string name, Action action, int milli) {
                _experiments.Add(new Experiment() { Name = name, TimeLimit = milli, RunExperiment = action });
                return this;
            }

            public ExperimentGroupBuilder AddCountLimitedExperiment(string name, Action action, int count) {
                _experiments.Add(new Experiment() { Name = name, Count = count, RunExperiment = action });
                return this;
            }

            public ExperimentRunner EndGroup() {
                _runner._groups.Add(new ExperimentGroup() { Name = _name, Experiments = _experiments });
                return _runner;
            }

            public ExperimentGroupBuilder(string name, ExperimentRunner runner) {
                _name = name;
                _runner = runner;
            }
        }

        public ExperimentGroupBuilder StartGroup(string name)
            => new ExperimentGroupBuilder(name, this);

        public ExperimentRunner Group(string name, Action<ExperimentGroupBuilder> builder) {
            var e = new ExperimentGroupBuilder(name, this);
            builder(e);
            return e.EndGroup();
        }

        public void RunGroupExperimentsParrallel(int warmup, string gname, int threadcount = 20) {
            Parallel.ForEach(Enumerable.Range(0, warmup), (i) => {
                for (int j = 0; i < j; ++i) ;
            });
            var g = _groups.FirstOrDefault((k) => k.Name == gname);
            if (g!= null) {
                foreach (var experiment in g.Experiments) {
                    try {
                        Console.WriteLine("Begin executing experiment.");
                        var result = experiment.RunParrallel(threadcount);
                        var er = new PExperimentResult();
                        er.ExperemimentName = experiment.Name;
                        er.Group = g.Name;
                        Console.WriteLine("Aggregating thread results");
                        er.ExecutionTimeMilliseconds = result.Max(f => f.ExecutionTimeMilliseconds);
                        er.ExecutionTimeSeconds = result.Max(f => f.ExecutionTimeSeconds);
                        er.ExperimentCount = result.Sum(f => f.ExperimentCount);
                        Program.Context.ReportPResult(er);
                      
                    } catch (Exception ex) {
                        Console.Error.WriteLine("EXCEPTION:/" + ex.Message);
                        Console.Error.WriteLine(ex.StackTrace);
                        Console.Error.WriteLine(ex.InnerException);
                    }
                }
            }
        }

        public void Run(int warmup) {
            Experiment e = new Experiment();
            e.RunExperiment = () => {
                int r = 2 + (3 * 6);
            };
            e.TimeLimit = warmup;
            e.Run();

            foreach (var g in _groups) {
                foreach (var experiment in g.Experiments) {
                    try {
                        experiment.Run();
                        var er = new ExperimentResult();
                        er.ExecutionTimeMilliseconds = experiment.Stopwatch.Elapsed.TotalMilliseconds;
                        er.ExecutionTimeSeconds = experiment.Stopwatch.Elapsed.TotalSeconds;
                        er.ExperemimentName = experiment.Name;
                        er.Group = g.Name;

                        if (experiment.TimeLimit == -1) {
                            er.ExecutionTime = experiment.Stopwatch.Elapsed.TotalMilliseconds;
                        } else {
                            er.ExperimentCount = long.Parse(experiment.RunnedExperiments.ToString());
                        }

                        Program.Context.ReportResult(er);
                    } catch (Exception ex) {
                        Console.Error.WriteLine("EXCEPTION:/" + ex.Message);
                    }
                   
                        
                }
                GC.Collect();
            }
        }
    }
}
